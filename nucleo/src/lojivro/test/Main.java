package lojivro.test;


import lojivro.util.ConverteDate;

import java.util.Date;

public class Main {

    public static void main(String args[]) {
        String strDate = "21/08/1995";

        System.out.println(strDate);
        System.out.println(strDate.getClass());

        Date utilDate = ConverteDate.StringToUtil(strDate);
        System.out.println(utilDate);
        System.out.println(utilDate.getClass());

        java.sql.Date sqlDate = ConverteDate.UtilToSql(utilDate);
        System.out.println(sqlDate);
        System.out.println(sqlDate.getClass());

        utilDate = ConverteDate.SqlToUtil(sqlDate);
        System.out.println(utilDate);
        System.out.println(utilDate.getClass());

        strDate = ConverteDate.UtilToString(utilDate);
        System.out.println(strDate);
        System.out.println(strDate.getClass());

    }
}
