package lojivro.test;


import lojivro.util.Conexao;
import lojivro.util.ConverteDate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

public class PopularBanco {

    public static void main(String args[]) {
        try {
            ArrayList<String> arrayList = new ArrayList<String>();

            String sql = "drop schema public cascade;";
            arrayList.add(sql);
            sql = "create schema public;";
            arrayList.add(sql);
            sql = "create table tb_usuario\n" +
                    "(\n" +
                    "\tid_usu serial not null,\n" +
                    "\temail varchar(64) not null,\n" +
                    "\tsenha varchar(32) not null,\n" +
                    "\tstatus boolean not null,\n" +
                    "\tdtcadastro_usu date not null,\n" +
                    "\tid_cli integer not null\n" +
                    ")\n" +
                    ";\n" +
                    "\n" +
                    "create unique index tb_usuario_email_uindex\n" +
                    "\ton tb_usuario (email)\n" +
                    ";\n" +
                    "\n" +
                    "create unique index tb_usuario_id_usu_uindex\n" +
                    "\ton tb_usuario (id_usu)\n" +
                    ";\n" +
                    "\n" +
                    "create unique index tb_usuario_id_cli_uindex\n" +
                    "\ton tb_usuario (id_cli)\n" +
                    ";\n" +
                    "\n" +
                    "create table tb_cartaocredito\n" +
                    "(\n" +
                    "\tid_ccr serial not null\n" +
                    "\t\tconstraint tb_cartaocredito_pkey\n" +
                    "\t\t\tprimary key,\n" +
                    "\tnumero varchar(32) not null,\n" +
                    "\tmes integer not null,\n" +
                    "\tano integer not null,\n" +
                    "\tcodseguranca char(8) not null,\n" +
                    "\tnomeimpresso varchar(32) not null,\n" +
                    "\tcliente integer not null,\n" +
                    "\tbandeira integer not null,\n" +
                    "\tdtcadastro_ccr date not null,\n" +
                    "\tcli_id integer not null\n" +
                    ")\n" +
                    ";\n" +
                    "\n" +
                    "create unique index tb_cartaocredito_id_uindex\n" +
                    "\ton tb_cartaocredito (id_ccr)\n" +
                    ";\n" +
                    "\n" +
                    "create table tb_bandeira\n" +
                    "(\n" +
                    "\tid_band serial not null\n" +
                    "\t\tconstraint tb_bandeira_pkey\n" +
                    "\t\t\tprimary key,\n" +
                    "\tbandeira varchar(32) not null,\n" +
                    "\tdtcadastro_band date not null\n" +
                    ")\n" +
                    ";\n" +
                    "\n" +
                    "create unique index tb_bandeira_bandeira_uindex\n" +
                    "\ton tb_bandeira (bandeira)\n" +
                    ";\n" +
                    "\n" +
                    "create unique index tb_bandeira_id_uindex\n" +
                    "\ton tb_bandeira (id_band)\n" +
                    ";\n" +
                    "\n" +
                    "create table tb_endereco\n" +
                    "(\n" +
                    "\tid_end serial not null\n" +
                    "\t\tconstraint tb_endereco_pkey\n" +
                    "\t\t\tprimary key,\n" +
                    "\ttiporesidencia integer not null,\n" +
                    "\tnumero varchar(8) not null,\n" +
                    "\tobservacoes text,\n" +
                    "\tcep integer not null,\n" +
                    "\tbairro varchar(64) not null,\n" +
                    "\ttipologradouro integer not null,\n" +
                    "\tlogradouro varchar(128) not null,\n" +
                    "\tcidade varchar(64) not null,\n" +
                    "\testado varchar(64) not null,\n" +
                    "\tpais varchar(64) not null,\n" +
                    "\tdtcadastro_end date not null,\n" +
                    "\tcli_id integer not null\n" +
                    ")\n" +
                    ";\n" +
                    "\n" +
                    "create unique index tb_endereco_id_end_uindex\n" +
                    "\ton tb_endereco (id_end)\n" +
                    ";\n" +
                    "\n";
            arrayList.add(sql);


            Connection connection = Conexao.getConnection();

            for (String s: arrayList) {
                PreparedStatement pst = connection.prepareStatement(s);
                pst.executeUpdate();
            };

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            System.out.print("Erro ao tentar abrir o banco!");
        }
    }
}
