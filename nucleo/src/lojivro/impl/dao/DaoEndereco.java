package lojivro.impl.dao;

import lojivro.EntidadeDominio;
import lojivro.entidades.infopessoas.endereco.*;
import lojivro.util.ConverteDate;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DaoEndereco extends DaoAbs {
    protected DaoEndereco() {
        super("endereco", "id_end");
    }

    @Override
    public void salvar(EntidadeDominio entidade) throws SQLException {
        openConnection();
        PreparedStatement pst = null;
        Endereco endereco = (Endereco) entidade;

        try {
            connection.setAutoCommit(false);

            String sql = "INSERT INTO tb_endereco(tiporesidencia, numero, observacoes, cep, bairro, tipologradouro, logradouro, cidade, estado, pais, dtcadastro)" +
                    " VALUES (?,?,?,?,?,?,?,?,?,?,?)";
            pst = connection.prepareStatement(sql // campo a ser inserido
                    , Statement.RETURN_GENERATED_KEYS);


            pst.setInt(1, endereco.getTipoResidencia().getId());
            pst.setString(2, endereco.getNumero());
            pst.setString(3, endereco.getObservacoes());
            pst.setInt(4, endereco.getLocalizacao().getCep());
            pst.setString(5, endereco.getLocalizacao().getBairro());
            pst.setInt(6, endereco.getLocalizacao().getTipoLogradouro().getId());
            pst.setString(7, endereco.getLocalizacao().getLogradouro());
            pst.setString(8, endereco.getLocalizacao().getCidade().getCidade());
            pst.setString(9, endereco.getLocalizacao().getCidade().getEstado().getEstado());
            pst.setString(10, endereco.getLocalizacao().getCidade().getEstado().getPais().getPais());
            pst.setDate(11, ConverteDate.UtilToSql(endereco.getDtCadastro()));

            pst.executeUpdate();

            ResultSet rs = pst.getGeneratedKeys();
            int id = 0;
            if (rs.next()) {
                id = rs.getInt("id_end");
            }
            endereco.setId(id);

            connection.commit();
        } catch (SQLException e) {
            runRollback(e);
        } finally {
            closeConnection(pst, ctrlTransaction);
        }
    }

    @Override
    public void alterar(EntidadeDominio entidade) throws SQLException {
        openConnection();
        PreparedStatement pst = null;
        Endereco endereco = (Endereco) entidade;

        try {
            connection.setAutoCommit(false);

            String sql = "UPDATE tb_endereco SET tiporesidencia=?, numero=?, observacoes=?, cep=?, bairro=?, tipologradouro=?, logradouro=?, cidade=?, estado=?, pais=?" +
                    " WHERE id_end = ?";
            pst = connection.prepareStatement(sql);

            pst.setInt(1, endereco.getTipoResidencia().getId());
            pst.setString(2, endereco.getNumero());
            pst.setString(3, endereco.getObservacoes());
            pst.setInt(4, endereco.getLocalizacao().getCep());
            pst.setString(5, endereco.getLocalizacao().getBairro());
            pst.setInt(6, endereco.getLocalizacao().getTipoLogradouro().getId());
            pst.setString(7, endereco.getLocalizacao().getLogradouro());
            pst.setString(8, endereco.getLocalizacao().getCidade().getCidade());
            pst.setString(9, endereco.getLocalizacao().getCidade().getEstado().getEstado());
            pst.setString(10, endereco.getLocalizacao().getCidade().getEstado().getPais().getPais());
            pst.setInt(11,endereco.getId());
            pst.executeUpdate();

            connection.commit();

        } catch (SQLException e) {
            runRollback(e);
        } finally {
            closeConnection(pst, ctrlTransaction);
        }
    }

    @Override
    public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
        PreparedStatement pst = null;
        Endereco endereco = (Endereco) entidade;

        String sql = "SELECT * FROM ((tb_endereco ende inner join tb_tipologradouro tlg on ende.tipologradouro = tlg.id_tlg) inner join tb_tiporesidencia trs on ende.tiporesidencia = trs.id_trs)";

        if (endereco.getLocalizacao().getLogradouro() == null) {
            endereco.getLocalizacao().setLogradouro("");
        }

        if (endereco.getId() != null && endereco.getLocalizacao().getLogradouro().equals("")) {
            sql += " WHERE id_end=?";
        } else if (endereco.getId() == null && !endereco.getLocalizacao().getLogradouro().equals("")) {
            sql += " WHERE logradouro like %?%";
        }

        try {
            openConnection();
            pst = connection.prepareStatement(sql);

            if (endereco.getId() != null && endereco.getId().equals("")) {
                pst.setInt(1, endereco.getId());
            } else if (endereco.getId() == null && !endereco.getLocalizacao().getLogradouro().equals("")) {
                pst.setString(1, endereco.getLocalizacao().getLogradouro());
            }

            ResultSet rs = pst.executeQuery();

            List<EntidadeDominio> enderecos = new ArrayList<EntidadeDominio>();
            while (rs.next()) {
                Endereco p = new Endereco();
                p.setLocalizacao(new Localizacao());
                p.setTipoResidencia(new TipoResidencia());
                p.getLocalizacao().setCidade(new Cidade());
                p.getLocalizacao().setTipoLogradouro(new TipoLogradouro());
                p.getLocalizacao().getCidade().setEstado(new Estado());
                p.getLocalizacao().getCidade().getEstado().setPais(new Pais());

                p.setId(rs.getInt("id_end"));
                p.setNumero(rs.getString("numero"));
                p.setObservacoes(rs.getString("observacoes"));
                p.setDtCadastro(ConverteDate.SqlToUtil(rs.getDate("dtcadastroend")));
                p.getTipoResidencia().setId(rs.getInt("id_trs"));
                p.getTipoResidencia().setTipo(rs.getString("tiporesidencia"));
                p.getTipoResidencia().setDtCadastro(ConverteDate.SqlToUtil(rs.getDate("dtcadastro_trs")));
                p.getLocalizacao().setLogradouro(rs.getString("logradouro"));
                p.getLocalizacao().setCep(rs.getInt("cep"));
                p.getLocalizacao().setBairro(rs.getString("bairro"));
                p.getLocalizacao().getTipoLogradouro().setId(rs.getInt("id_tlg"));
                p.getLocalizacao().getTipoLogradouro().setTipo(rs.getString("tipologradouro"));
                p.getLocalizacao().getTipoLogradouro().setDtCadastro(ConverteDate.SqlToUtil(rs.getDate("dtcadastro_tlg")));
                p.getLocalizacao().getCidade().setCidade(rs.getString("cidade"));
                p.getLocalizacao().getCidade().getEstado().setEstado(rs.getString("estado"));
                p.getLocalizacao().getCidade().getEstado().getPais().setPais(rs.getString("pais"));

                enderecos.add(p);
            }
            return enderecos;
        } catch (SQLException e) {
            runRollback(e);
        } finally{ // Executa o bloco abaixo independente do que ocorreu
            closeConnection(pst,ctrlTransaction);
        }
        return null;
    }
}
