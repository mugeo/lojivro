package lojivro.impl.dao;

import lojivro.EntidadeDominio;
import lojivro.entidades.pessoas.Usuario;
import lojivro.util.ConverteDate;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DaoUsuario extends DaoAbs {
    protected DaoUsuario() {
        super("tb_usuario", "id_usu");
    }

    @Override
    public void salvar(EntidadeDominio entidade) throws SQLException {

        openConnection();
        PreparedStatement pst = null;
        Usuario usuario = (Usuario) entidade;

        try {
            connection.setAutoCommit(false);

            String sql = "INSERT INTO tb_usuario(email, senha, status, dtcadastro)" +
                    " VALUES (?, ?, ?, ?)";
            pst = connection.prepareStatement(sql // campo a ser inserido
                    , Statement.RETURN_GENERATED_KEYS);

            pst.setString(1, usuario.getEmail());
            pst.setString(2, usuario.getSenha());
            pst.setBoolean(3, usuario.isStatus());
            pst.setDate(4, ConverteDate.UtilToSql(usuario.getDataCadastro()));
            pst.executeUpdate();

            ResultSet rs = pst.getGeneratedKeys();
            int id = 0;
            if (rs.next()) {
                id = rs.getInt("id_usu");
            }
            usuario.setId(id);

            connection.commit();
        } catch (SQLException e) {
            runRollback(e);
        } finally {
            closeConnection(pst, ctrlTransaction);
        }
    }

    @Override
    public void alterar(EntidadeDominio entidade) throws SQLException {
        openConnection();
        PreparedStatement pst = null;
        Usuario usuario = (Usuario) entidade;

        try {
            connection.setAutoCommit(false);

            String sql = "UPDATE tb_usuario SET email=?, senha=?, status=?" +
                    " WHERE id_usu = ?";
            pst = connection.prepareStatement(sql);

            pst.setString(1, usuario.getEmail());
            pst.setString(2, usuario.getSenha());
            pst.setBoolean(3, usuario.isStatus());
            pst.setInt(4, usuario.getId());
            pst.executeUpdate();

            connection.commit();

        } catch (SQLException e) {
            runRollback(e);
        } finally {
            closeConnection(pst, ctrlTransaction);
        }
    }

    @Override
    public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
        PreparedStatement pst = null;
        Usuario usuario = (Usuario) entidade;

        String sql = "SELECT * FROM tb_usuario";

        int parametrizador = 0;
        if (usuario.getEmail() == null) {
            usuario.setEmail("");
        }

        if (usuario.getId() != null && usuario.getEmail().equals("")) {
            sql += " WHERE id_usu=?";
        } else if (usuario.getId() == null && !usuario.getEmail().equals("")) {
            sql += " WHERE email=?";
        } else if (usuario.getId() == null && usuario.getEmail().equals("")) {
            sql += " WHERE status=?";
        }

        try {
            openConnection();
            pst = connection.prepareStatement(sql);

            if (usuario.getId() != null && usuario.getEmail().equals("")) {
                pst.setInt(1, usuario.getId());
            } else if (usuario.getId() == null && !usuario.getEmail().equals("")) {
                pst.setString(1, usuario.getEmail());
            } else if (usuario.getId() == null && usuario.getEmail().equals("")) {
                pst.setBoolean(1, usuario.isStatus());
            }

            ResultSet rs = pst.executeQuery();
            List<EntidadeDominio> usuarios = new ArrayList<EntidadeDominio>();
            while (rs.next()) {
                Usuario p = new Usuario();
                p.setId(rs.getInt("id_usu"));
                p.setEmail(rs.getString("email"));
                p.setSenha(rs.getString("senha"));
                p.setStatus(rs.getBoolean("status"));
                p.setDataCadastro(ConverteDate.SqlToUtil(rs.getDate("datacadastro")));

                usuarios.add(p);
            }
            return usuarios;
        } catch (SQLException e) {
            runRollback(e);
        } finally{ // Executa o bloco abaixo independente do que ocorreu
            closeConnection(pst,ctrlTransaction);
        }
        return null;
    }
}
