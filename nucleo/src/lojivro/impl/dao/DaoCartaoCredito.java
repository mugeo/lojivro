package lojivro.impl.dao;

import lojivro.EntidadeDominio;
import lojivro.entidades.infopessoas.cartoes.Bandeira;
import lojivro.entidades.infopessoas.cartoes.CartaoCredito;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DaoCartaoCredito extends DaoAbs{

    protected DaoCartaoCredito() { super("tb_cartaocredito", "id_ccr");
    }

    @Override
    public void salvar(EntidadeDominio entidade) throws SQLException {
        openConnection();
        PreparedStatement pst = null;
        CartaoCredito cartaoCredito = (CartaoCredito) entidade;

        try {
            connection.setAutoCommit(false);

            String sql = "INSERT INTO tb_cartaocredito(numero, mes, ano, nomeimpresso, codseguranca, bandeira)" +
                    " VALUES (?, ?, ?, ?, ?, ?)";
            pst = connection.prepareStatement(sql // campo a ser inserido
                    , Statement.RETURN_GENERATED_KEYS);

            pst.setString(1, cartaoCredito.getNumero());
            pst.setInt(2, cartaoCredito.getMes());
            pst.setInt(3, cartaoCredito.getAno());
            pst.setString(4, cartaoCredito.getNomeImpresso());
            pst.setString(5, cartaoCredito.getCodSeguranca());
            pst.setInt(6, cartaoCredito.getBandeira().getId());

            pst.executeUpdate();

            ResultSet rs = pst.getGeneratedKeys();
            int id = 0;
            if (rs.next()) {
                id = rs.getInt("id_ccr");
            }
            cartaoCredito.setId(id);

            connection.commit();
        } catch (SQLException e) {
            runRollback(e);
        } finally {
            closeConnection(pst, ctrlTransaction);
        }
    }

    @Override
    public void alterar(EntidadeDominio entidade) throws SQLException {
        openConnection();
        PreparedStatement pst = null;
        CartaoCredito cartaoCredito = (CartaoCredito) entidade;

        try {
            connection.setAutoCommit(false);

            String sql = "UPDATE tb_cartaocredito SET numero=?, mes=?, ano=?, nomeimpresso=?, codseguranca=?, bandeira=?" +
                    " WHERE id_ccr=?";
            pst = connection.prepareStatement(sql);

            pst.setString(1, cartaoCredito.getNumero());
            pst.setInt(2, cartaoCredito.getMes());
            pst.setInt(3, cartaoCredito.getAno());
            pst.setString(4, cartaoCredito.getNomeImpresso());
            pst.setString(5, cartaoCredito.getCodSeguranca());
            pst.setInt(6, cartaoCredito.getBandeira().getId());
            pst.setInt(7, cartaoCredito.getId());
            pst.executeUpdate();

            connection.commit();

        } catch (SQLException e) {
            runRollback(e);
        } finally {
            closeConnection(pst, ctrlTransaction);
        }
    }

    @Override
    public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
        PreparedStatement pst = null;
        CartaoCredito cartaoCredito = (CartaoCredito) entidade;

        String sql = "SELECT cc.id as id, cc.numero as numero, cc.mes as mes, cc.ano as ano, cc.codseguranca as codseguranca, cc.nomeimpresso as nomeimpresso, cc.cliente as cliente, cc.bandeira as idbandeira, bd.bandeira as bandeira FROM tb_cartaocredito cc inner join tb_bandeira bd on cc.bandeira = bd.id";

        if (cartaoCredito.getNumero() == null) {
            cartaoCredito.setNumero("");
        }

        if (cartaoCredito.getId() != null && cartaoCredito.getNumero().equals("")) {
            sql += " WHERE id_ccr=?";
        } else if (cartaoCredito.getId() == null && !cartaoCredito.getNumero().equals("")) {
            sql += " WHERE numero=?";
        }

        try {
            openConnection();
            pst = connection.prepareStatement(sql);

            if (cartaoCredito.getId() != null && cartaoCredito.getNumero().equals("")) {
                pst.setInt(1, cartaoCredito.getId());
            } else if (cartaoCredito.getId() == null && !cartaoCredito.getNumero().equals("")) {
                pst.setString(1, cartaoCredito.getNumero());
            }

            ResultSet rs = pst.executeQuery();

            List<EntidadeDominio> cartoes = new ArrayList<EntidadeDominio>();
            while (rs.next()) {
                CartaoCredito p = new CartaoCredito();
                p.setId(rs.getInt("id_ccr"));
                p.setNumero(rs.getString("numero"));
                p.setMes(rs.getInt("mes"));
                p.setAno(rs.getInt("ano"));
                p.setCodSeguranca(rs.getString("codseguranca"));
                p.setNomeImpresso(rs.getString("nomeimpresso"));
                p.setBandeira(new Bandeira());
                p.getBandeira().setId(rs.getInt("idbandeira"));
                p.getBandeira().setBandeira(rs.getString("bandeira"));

                cartoes.add(p);
            }
            return cartoes;
        } catch (SQLException e) {
            runRollback(e);
        } finally { // Executa o bloco abaixo independente do que ocorreu
            closeConnection(pst, ctrlTransaction);
        }
        return null;
    }
}
