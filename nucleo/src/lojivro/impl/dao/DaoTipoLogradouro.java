package lojivro.impl.dao;

import lojivro.EntidadeDominio;
import lojivro.entidades.infopessoas.endereco.TipoLogradouro;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DaoTipoLogradouro extends DaoAbs{

    protected DaoTipoLogradouro() {
        super("tb_tipologradouro", "id_tlg");
    }

    @Override
    public void salvar(EntidadeDominio entidade) throws SQLException {
        openConnection();
        PreparedStatement pst = null;
        TipoLogradouro tipoTelefone = (TipoLogradouro) entidade;

        try {
            connection.setAutoCommit(false);

            String sql = "INSERT INTO tb_tipologradouro(tipologradouro)" +
                    " VALUES (?)";
            pst = connection.prepareStatement(sql // campo a ser inserido
                    , Statement.RETURN_GENERATED_KEYS);

            pst.setString(1, tipoTelefone.getTipo());

            pst.executeUpdate();

            ResultSet rs = pst.getGeneratedKeys();
            int id = 0;
            if (rs.next()) {
                id = rs.getInt("id_tlg");
            }
            tipoTelefone.setId(id);

            connection.commit();
        } catch (SQLException e) {
            runRollback(e);
        } finally {
            closeConnection(pst, ctrlTransaction);
        }
    }

    @Override
    public void alterar(EntidadeDominio entidade) throws SQLException {
        openConnection();
        PreparedStatement pst = null;
        TipoLogradouro tipoTelefone = (TipoLogradouro) entidade;

        try {
            connection.setAutoCommit(false);

            String sql = "UPDATE tb_tipologradouro SET tipologradouro=?" +
                    " WHERE id_tlg=?";
            pst = connection.prepareStatement(sql);

            pst.setString(1, tipoTelefone.getTipo());
            pst.setInt(2, tipoTelefone.getId());
            pst.executeUpdate();

            connection.commit();

        } catch (SQLException e) {
            runRollback(e);
        } finally {
            closeConnection(pst, ctrlTransaction);
        }
    }

    @Override
    public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {

        PreparedStatement pst = null;
        TipoLogradouro tipologradouro = (TipoLogradouro) entidade;

        String sql = "SELECT * FROM tb_tipologradouro";

        if (tipologradouro.getTipo() == null) {
            tipologradouro.setTipo("");
        }

        if (tipologradouro.getId() != null && tipologradouro.getTipo().equals("")) {
            sql += " WHERE id_tlg=?";
        } else if (tipologradouro.getId() == null && !tipologradouro.getTipo().equals("")) {
            sql += " WHERE tipo=?";
        }

        try {
            openConnection();
            pst = connection.prepareStatement(sql);

            if (tipologradouro.getId() != null && tipologradouro.getTipo().equals("")) {
                pst.setInt(1, tipologradouro.getId());
            } else if (tipologradouro.getId() == null && !tipologradouro.getTipo().equals("")) {
                pst.setString(1, tipologradouro.getTipo());
            }

            ResultSet rs = pst.executeQuery();

            List<EntidadeDominio> tipostelefone = new ArrayList<EntidadeDominio>();
            while (rs.next()) {
                TipoLogradouro p = new TipoLogradouro();
                p.setId(rs.getInt("id_tlg"));
                p.setTipo(rs.getString("tipologradouro"));

                tipostelefone.add(p);
            }
            return tipostelefone;
        } catch (SQLException e) {
            runRollback(e);
        } finally { // Executa o bloco abaixo independente do que ocorreu
            closeConnection(pst, ctrlTransaction);
        }
        return null;
    }
}
