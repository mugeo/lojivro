package lojivro.impl.dao;

import lojivro.EntidadeDominio;
import lojivro.entidades.infopessoas.cartoes.Bandeira;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DaoBandeira extends DaoAbs{
    public DaoBandeira() {
        super("tb_bandeira", "id_band");
    }

    @Override
    public void salvar(EntidadeDominio entidade) throws SQLException {
        openConnection();
        PreparedStatement pst = null;
        Bandeira bandeira = (Bandeira) entidade;

        try {
            connection.setAutoCommit(false);

            String sql = "INSERT INTO tb_bandeira(bandeira)" +
                    " VALUES (?)";
            pst = connection.prepareStatement(sql // campo a ser inserido
                    , Statement.RETURN_GENERATED_KEYS);

            pst.setString(1, bandeira.getBandeira());

            pst.executeUpdate();

            ResultSet rs = pst.getGeneratedKeys();
            int id = 0;
            if (rs.next()) {
                id = rs.getInt("id_band");
            }
            bandeira.setId(id);

            connection.commit();
        } catch (SQLException e) {
            runRollback(e);
        } finally {
            closeConnection(pst, ctrlTransaction);
        }
    }

    @Override
    public void alterar(EntidadeDominio entidade) throws SQLException {
        openConnection();
        PreparedStatement pst = null;
        Bandeira bandeira = (Bandeira) entidade;

        try {
            connection.setAutoCommit(false);

            String sql = "UPDATE tb_bandeira SET bandeira=?" +
                    " WHERE id_band = ?";
            pst = connection.prepareStatement(sql);

            pst.setString(1, bandeira.getBandeira());
            pst.setInt(2, bandeira.getId());
            pst.executeUpdate();

            connection.commit();

        } catch (SQLException e) {
            runRollback(e);
        } finally {
            closeConnection(pst, ctrlTransaction);
        }
    }

    @Override
    public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
        PreparedStatement pst = null;
        Bandeira bandeira = (Bandeira) entidade;

        String sql = "SELECT * FROM tb_bandeira";

        if (bandeira.getBandeira() == null) {
            bandeira.setBandeira("");
        }

        if (bandeira.getId() != null && bandeira.getBandeira().equals("")) {
            sql += " WHERE id_band=?";
        } else if (bandeira.getId() == null && !bandeira.getBandeira().equals("")) {
            sql += " WHERE bandeira=?";
        }

        try {
            openConnection();
            pst = connection.prepareStatement(sql);

            if (bandeira.getId() != null && bandeira.getBandeira().equals("")) {
                pst.setInt(1, bandeira.getId());
            } else if (bandeira.getId() == null && !bandeira.getBandeira().equals("")) {
                pst.setString(1, bandeira.getBandeira());
            }

            ResultSet rs = pst.executeQuery();

            List<EntidadeDominio> bandeiras = new ArrayList<EntidadeDominio>();
            while (rs.next()) {
                Bandeira p = new Bandeira();
                p.setId(rs.getInt("id_band"));
                p.setBandeira(rs.getString("bandeira"));

                bandeiras.add(p);
            }
            return bandeiras;
        } catch (SQLException e) {
            runRollback(e);
        } finally{ // Executa o bloco abaixo independente do que ocorreu
            closeConnection(pst,ctrlTransaction);
        }
        return null;
    }
}
