package lojivro.impl.dao;

import lojivro.EntidadeDominio;
import lojivro.entidades.livro.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DaoLivro extends DaoAbs {

    public DaoLivro() {
        super("tb_livro", "id_livro");
    }

    public void salvar(EntidadeDominio entidade) {
        openConnection();
        PreparedStatement pst = null;
        Livro livro = (Livro) entidade;

        try {
            connection.setAutoCommit(false);

            StringBuilder sql = new StringBuilder();
            sql.append(
                    "INSERT INTO tb_livro(titulo, ano, edicao, isbn, numero_pagina, sinopse, altura, largura, peso, profundidade, codigo_barras, fk_grupo, fk_editora, status)");
            sql.append(" VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"); // campo a ser inserido

            pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);

            pst.setString(1, livro.getTitulo());
            pst.executeUpdate();

            ResultSet rs = pst.getGeneratedKeys();
            int id = 0;
            if (rs.next())
                id = rs.getInt(1);
            livro.setId(id);

            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                pst.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void alterar(EntidadeDominio entidade) {
        openConnection();
        PreparedStatement pst = null;
        Livro livro = (Livro) entidade;

        try {
            connection.setAutoCommit(false);

            StringBuilder sql = new StringBuilder();
            sql.append(
                    "UPDATE tb_livro SET (titulo, ano, edicao, isbn, numero_pagina, sinopse, altura, largura, peso, profundidade, codigo_barras, fk_grupo, fk_editora, status)");
            sql.append(" = (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            sql.append(" WHERE id_livro = ?");

            pst = connection.prepareStatement(sql.toString());

            pst.setString(1, livro.getTitulo());
            pst.setInt(15, livro.getId());
            pst.executeUpdate();

            connection.commit();

        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                pst.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    // metodo inativar
    public void inativar(EntidadeDominio entidade) {
        openConnection();
        PreparedStatement pst = null;
        Livro livro = (Livro) entidade;

        try {
            connection.setAutoCommit(false);

            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE tb_livro SET (status)");
            sql.append(" = (?)");
            sql.append(" WHERE id_livro = ?");

            pst = connection.prepareStatement(sql.toString());

            connection.commit();

        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                pst.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public List<EntidadeDominio> consultar(EntidadeDominio entidade) {
        PreparedStatement pst = null;
        Livro livro = (Livro) entidade;
        if (livro == null) {
            livro = new Livro();
        }

        String sql = null;

        if (livro.getTitulo() == null) {
            livro.setTitulo("");
        }

        if (livro.getId() == null && livro.getTitulo().equals("")) {
            sql = "SELECT * FROM tb_livro WHERE status = ?";
        } else if (livro.getId() != null && livro.getTitulo().equals("")) {
            sql = "SELECT * FROM tb_livro WHERE id_livro=? and status = ?";
        } else if (livro.getId() == null && !livro.getTitulo().equals("")) {
            sql = "SELECT * FROM tb_livro WHERE titulo ilike ? and status = ?";

        }

        try {
            openConnection();
            pst = connection.prepareStatement(sql);

            if (livro.getId() == null && livro.getTitulo().equals("")) {
                pst.setBoolean(1, true);
            } else if (livro.getId() != null && livro.getTitulo().equals("")) {
                pst.setInt(1, livro.getId());
                pst.setBoolean(2, true);
            } else if (livro.getId() == null && !livro.getTitulo().equals("")) {
                pst.setString(1, "%" + livro.getTitulo() + "%");
                pst.setBoolean(2, true);
            }

            ResultSet rs = pst.executeQuery();
            List<EntidadeDominio> livros = new ArrayList<EntidadeDominio>();
            while (rs.next()) {
                Livro p = new Livro();
                p.setId(rs.getInt("id_livro"));
                p.setTitulo(rs.getString("titulo"));
                p.setSinopse(rs.getString("sinopse"));

                livros.add(p);
            }
            return livros;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally // Executa o bloco abaixo independente do que ocorreu
        {
            try // Ocorreu um erro de SQLException
            {
                pst.close(); // Nao sera mais utilizado. Fecha o
                connection.close(); // Fecha a conex�o com o BD
            } catch (SQLException e) // Ocorreu um erro no fechamento da conexao
            {
                e.printStackTrace(); // Exibe uma mensagem com a excecao
            }
        }
        return null;
    }

    // metodo get autor
    public List<EntidadeDominio> getAutor(EntidadeDominio entidade) {
        PreparedStatement pst = null;
        String sql = null;

        sql = "SELECT * FROM tb_autor";
        try {
            openConnection();
            pst = connection.prepareStatement(sql);

            ResultSet rs = pst.executeQuery();
            List<EntidadeDominio> autores = new ArrayList<EntidadeDominio>();
            while (rs.next()) {
                Autor a = new Autor();

                autores.add(a);
            }
            return autores;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally // Executa o bloco abaixo independente do que ocorreu
        {
            try // Ocorreu um erro de SQLException
            {
                pst.close(); // Nao sera mais utilizado. Fecha o
                connection.close(); // Fecha a conex�o com o BD
            } catch (SQLException e) // Ocorreu um erro no fechamento da conexao
            {
                e.printStackTrace(); // Exibe uma mensagem com a excecao
            }
        }
        return null;
    }

    // metodo get editora
    public List<EntidadeDominio> getEditora(EntidadeDominio entidade) {
        PreparedStatement pst = null;
        String sql = null;

        sql = "SELECT * FROM tb_editora";
        try {
            openConnection();
            pst = connection.prepareStatement(sql);

            ResultSet rs = pst.executeQuery();
            List<EntidadeDominio> editoras = new ArrayList<EntidadeDominio>();
            while (rs.next()) {
                Editora a = new Editora();
                a.setId(rs.getInt("id_editora"));
                a.setNomeEditora(rs.getString("nome_editora"));

                editoras.add(a);
            }
            return editoras;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally // Executa o bloco abaixo independente do que ocorreu
        {
            try // Ocorreu um erro de SQLException
            {
                pst.close(); // Nao sera mais utilizado. Fecha o
                connection.close(); // Fecha a conex�o com o BD
            } catch (SQLException e) // Ocorreu um erro no fechamento da conexao
            {
                e.printStackTrace(); // Exibe uma mensagem com a excecao
            }
        }
        return null;
    }

    // metodo get categoria
    public List<EntidadeDominio> getCategoria(EntidadeDominio entidade) {
        PreparedStatement pst = null;
        String sql = null;

        sql = "SELECT * FROM tb_categoria_livro";
        try {
            openConnection();
            pst = connection.prepareStatement(sql);

            ResultSet rs = pst.executeQuery();
            List<EntidadeDominio> categorias = new ArrayList<EntidadeDominio>();
            while (rs.next()) {
                Categoria a = new Categoria();
                a.setId(rs.getInt("id_categoria"));

                categorias.add(a);
            }
            return categorias;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally // Executa o bloco abaixo independente do que ocorreu
        {
            try // Ocorreu um erro de SQLException
            {
                pst.close(); // Nao sera mais utilizado. Fecha o
                connection.close(); // Fecha a conex�o com o BD
            } catch (SQLException e) // Ocorreu um erro no fechamento da conexao
            {
                e.printStackTrace(); // Exibe uma mensagem com a excecao
            }
        }
        return null;
    }

    // metodo get grupo
    public List<EntidadeDominio> getGrupos(EntidadeDominio entidade) {
        PreparedStatement pst = null;
        String sql = null;

        sql = "SELECT * FROM tb_grupo_precificacao";
        try {
            openConnection();
            pst = connection.prepareStatement(sql);

            ResultSet rs = pst.executeQuery();
            List<EntidadeDominio> grupos = new ArrayList<EntidadeDominio>();
            while (rs.next()) {
                GrupoPrecificacao a = new GrupoPrecificacao();
                a.setId(rs.getInt("id_grupo"));
                a.setNomeGrupo(rs.getString("nome_grupo"));
                a.setPorcentagem_lucro(rs.getDouble("porcentagem_lucro"));

                grupos.add(a);
            }
            return grupos;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally // Executa o bloco abaixo independente do que ocorreu
        {
            try // Ocorreu um erro de SQLException
            {
                pst.close(); // Nao sera mais utilizado. Fecha o
                connection.close(); // Fecha a conex�o com o BD
            } catch (SQLException e) // Ocorreu um erro no fechamento da conexao
            {
                e.printStackTrace(); // Exibe uma mensagem com a excecao
            }
        }
        return null;
    }
}