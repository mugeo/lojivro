package lojivro.impl.dao;

import lojivro.EntidadeDominio;
import lojivro.entidades.pessoas.Cliente;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class DaoCliente extends DaoAbs {

    public DaoCliente() {
        super("tb_cliente", "id_cli");
    }

    public void salvar(EntidadeDominio entidade) {
        openConnection();
        PreparedStatement pst = null;
        Cliente cliente = (Cliente) entidade;

        try {
            connection.setAutoCommit(false);

            String sql = "INSERT INTO tb_cliente(nome, cpf, " +
                    "dt_cadastro) VALUES (?,?,?)";
            pst = connection.prepareStatement(sql);
            pst.setString(1, cliente.getNome());
            pst.setString(2, cliente.getCpf());
            //Timestamp time = new Timestamp(cliente.getDtCadastro().getTime());
            //pst.setTimestamp(3, time);
            pst.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            runRollback(e);
        } finally {
            closeConnection(pst, ctrlTransaction);
        }

    }


    @Override
    public void alterar(EntidadeDominio entidade) {
        // TODO Auto-generated method stub

    }


    @Override
    public List<EntidadeDominio> consultar(EntidadeDominio entidade) {
        // TODO Auto-generated method stub
        return null;
    }


}
