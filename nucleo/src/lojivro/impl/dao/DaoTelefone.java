package lojivro.impl.dao;

import lojivro.EntidadeDominio;
import lojivro.entidades.infopessoas.contato.Telefone;
import lojivro.entidades.infopessoas.contato.TipoTelefone;
import lojivro.util.ConverteDate;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DaoTelefone extends DaoAbs {
    protected DaoTelefone() {
        super("tb_telefone", "id_tel");
    }

    @Override
    public void salvar(EntidadeDominio entidade) throws SQLException {
        openConnection();
        PreparedStatement pst = null;
        Telefone telefone = (Telefone) entidade;

        try {
            connection.setAutoCommit(false);

            String sql = "INSERT INTO tb_telefone(ddd, telefone, tipotelefone)" +
                    " VALUES (?, ?, ?)";
            pst = connection.prepareStatement(sql // campo a ser inserido
                    , Statement.RETURN_GENERATED_KEYS);

            pst.setInt(1, telefone.getDdd());
            pst.setString(2, telefone.getTelefone());
            pst.setInt(3, telefone.getTipoTelefone().getId());

            pst.executeUpdate();

            ResultSet rs = pst.getGeneratedKeys();
            int id = 0;
            if (rs.next()) {
                id = rs.getInt("id_tel");
            }
            telefone.setId(id);

            connection.commit();
        } catch (SQLException e) {
            runRollback(e);
        } finally {
            closeConnection(pst, ctrlTransaction);
        }
    }

    @Override
    public void alterar(EntidadeDominio entidade) throws SQLException {
        openConnection();
        PreparedStatement pst = null;
        Telefone telefone = (Telefone) entidade;

        try {
            connection.setAutoCommit(false);

            String sql = "UPDATE tb_telefone SET ddd=?, telefone=?, tipotelefone=?" +
                    " WHERE id_tel = ?";
            pst = connection.prepareStatement(sql);

            pst.setInt(1, telefone.getDdd());
            pst.setString(2, telefone.getTelefone());
            pst.setInt(3, telefone.getTipoTelefone().getId());
            pst.setInt(4, telefone.getId());
            pst.executeUpdate();

            connection.commit();

        } catch (SQLException e) {
            runRollback(e);
        } finally {
            closeConnection(pst, ctrlTransaction);
        }
    }

    @Override
    public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {
        PreparedStatement pst = null;
        Telefone telefone = (Telefone) entidade;

        String sql = "SELECT * FROM tb_telefone";

        if (telefone.getTelefone() == null) {
            telefone.setTelefone("");
        }

        if (telefone.getId() != null && telefone.getTelefone().equals("")) {
            sql += " WHERE id_tel=?";
        } else if (telefone.getId() == null && !telefone.getTelefone().equals("")) {
            sql += " WHERE telefone=?";
        } else if (telefone.getId() == null && telefone.getTelefone().equals("")) {
            sql += " WHERE ddd=?";
        }

        try {
            openConnection();
            pst = connection.prepareStatement(sql);

            if (telefone.getId() != null && telefone.getTelefone().equals("")) {
                pst.setInt(1, telefone.getId());
            } else if (telefone.getId() == null && !telefone.getTelefone().equals("")) {
                pst.setString(1, telefone.getTelefone());
            } else if (telefone.getId() == null && telefone.getTelefone().equals("")) {
                pst.setInt(1, telefone.getDdd());
            }

            ResultSet rs = pst.executeQuery();

            DaoTipoTelefone daoTipoTelefone = new DaoTipoTelefone();
            List<EntidadeDominio> telefones = new ArrayList<EntidadeDominio>();
            while (rs.next()) {
                Telefone p = new Telefone();
                p.setId(rs.getInt("id_tel"));
                p.setTelefone(rs.getString("telefone"));
                p.setDdd(rs.getInt("ddd"));

                TipoTelefone tipoTelefone = new TipoTelefone();
                tipoTelefone.setId(rs.getInt("tipotelefone"));
                p.setTipoTelefone(new TipoTelefone());
                p.setTipoTelefone((TipoTelefone)(daoTipoTelefone.consultar(tipoTelefone).get(0)));

                telefones.add(p);
            }
            return telefones;
        } catch (SQLException e) {
            runRollback(e);
        } finally{ // Executa o bloco abaixo independente do que ocorreu
            closeConnection(pst,ctrlTransaction);
        }
        return null;
    }
}
