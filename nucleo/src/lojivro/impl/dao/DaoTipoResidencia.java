package lojivro.impl.dao;

import lojivro.EntidadeDominio;
import lojivro.entidades.infopessoas.endereco.TipoResidencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DaoTipoResidencia extends DaoAbs{

    protected DaoTipoResidencia() {
        super("tb_tiporesidencia", "id_trs");
    }

    @Override
    public void salvar(EntidadeDominio entidade) throws SQLException {
        openConnection();
        PreparedStatement pst = null;
        TipoResidencia tipoTelefone = (TipoResidencia) entidade;

        try {
            connection.setAutoCommit(false);

            String sql = "INSERT INTO tb_tiporesidencia(tiporesidencia)" +
                    " VALUES (?)";
            pst = connection.prepareStatement(sql // campo a ser inserido
                    , Statement.RETURN_GENERATED_KEYS);

            pst.setString(1, tipoTelefone.getTipo());

            pst.executeUpdate();

            ResultSet rs = pst.getGeneratedKeys();
            int id = 0;
            if (rs.next()) {
                id = rs.getInt("id_trs");
            }
            tipoTelefone.setId(id);

            connection.commit();
        } catch (SQLException e) {
            runRollback(e);
        } finally {
            closeConnection(pst, ctrlTransaction);
        }
    }

    @Override
    public void alterar(EntidadeDominio entidade) throws SQLException {
        openConnection();
        PreparedStatement pst = null;
        TipoResidencia tipoTelefone = (TipoResidencia) entidade;

        try {
            connection.setAutoCommit(false);

            String sql = "UPDATE tb_tiporesidencia SET tiporesidencia=?" +
                    " WHERE id_trs=?";
            pst = connection.prepareStatement(sql);

            pst.setString(1, tipoTelefone.getTipo());
            pst.setInt(2, tipoTelefone.getId());
            pst.executeUpdate();

            connection.commit();

        } catch (SQLException e) {
            runRollback(e);
        } finally {
            closeConnection(pst, ctrlTransaction);
        }
    }

    @Override
    public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {

        PreparedStatement pst = null;
        TipoResidencia tiporesidencia = (TipoResidencia) entidade;

        String sql = "SELECT * FROM tb_tiporesidencia";

        if (tiporesidencia.getTipo() == null) {
            tiporesidencia.setTipo("");
        }

        if (tiporesidencia.getId() != null && tiporesidencia.getTipo().equals("")) {
            sql += " WHERE id_trs=?";
        } else if (tiporesidencia.getId() == null && !tiporesidencia.getTipo().equals("")) {
            sql += " WHERE tipo=?";
        }

        try {
            openConnection();
            pst = connection.prepareStatement(sql);

            if (tiporesidencia.getId() != null && tiporesidencia.getTipo().equals("")) {
                pst.setInt(1, tiporesidencia.getId());
            } else if (tiporesidencia.getId() == null && !tiporesidencia.getTipo().equals("")) {
                pst.setString(1, tiporesidencia.getTipo());
            }

            ResultSet rs = pst.executeQuery();

            List<EntidadeDominio> tipostelefone = new ArrayList<EntidadeDominio>();
            while (rs.next()) {
                TipoResidencia p = new TipoResidencia();
                p.setId(rs.getInt("id_trs"));
                p.setTipo(rs.getString("tiporesidencia"));

                tipostelefone.add(p);
            }
            return tipostelefone;
        } catch (SQLException e) {
            runRollback(e);
        } finally { // Executa o bloco abaixo independente do que ocorreu
            closeConnection(pst, ctrlTransaction);
        }
        return null;
    }
}
