package lojivro.impl.dao;

import lojivro.EntidadeDominio;
import lojivro.entidades.infopessoas.contato.TipoTelefone;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DaoTipoTelefone extends DaoAbs{

    protected DaoTipoTelefone() {
        super("tb_tipotelefone", "id_ttel");
    }

    @Override
    public void salvar(EntidadeDominio entidade) throws SQLException {
        openConnection();
        PreparedStatement pst = null;
        TipoTelefone tipoTelefone = (TipoTelefone) entidade;

        try {
            connection.setAutoCommit(false);

            String sql = "INSERT INTO tb_tipotelefone(tipotelefone)" +
                    " VALUES (?)";
            pst = connection.prepareStatement(sql // campo a ser inserido
                    , Statement.RETURN_GENERATED_KEYS);

            pst.setString(1, tipoTelefone.getTipo());

            pst.executeUpdate();

            ResultSet rs = pst.getGeneratedKeys();
            int id = 0;
            if (rs.next()) {
                id = rs.getInt("id_ttel");
            }
            tipoTelefone.setId(id);

            connection.commit();
        } catch (SQLException e) {
            runRollback(e);
        } finally {
            closeConnection(pst, ctrlTransaction);
        }
    }

    @Override
    public void alterar(EntidadeDominio entidade) throws SQLException {
        openConnection();
        PreparedStatement pst = null;
        TipoTelefone tipoTelefone = (TipoTelefone) entidade;

        try {
            connection.setAutoCommit(false);

            String sql = "UPDATE tb_tipotelefone SET tipotelefone=?" +
                    " WHERE id_ttel=?";
            pst = connection.prepareStatement(sql);

            pst.setString(1, tipoTelefone.getTipo());
            pst.setInt(2, tipoTelefone.getId());
            pst.executeUpdate();

            connection.commit();

        } catch (SQLException e) {
            runRollback(e);
        } finally {
            closeConnection(pst, ctrlTransaction);
        }
    }

    @Override
    public List<EntidadeDominio> consultar(EntidadeDominio entidade) throws SQLException {

        PreparedStatement pst = null;
        TipoTelefone tipotelefone = (TipoTelefone) entidade;

        String sql = "SELECT * FROM tb_tipotelefone";

        if (tipotelefone.getTipo() == null) {
            tipotelefone.setTipo("");
        }

        if (tipotelefone.getId() != null && tipotelefone.getTipo().equals("")) {
            sql += " WHERE id_ttel=?";
        } else if (tipotelefone.getId() == null && !tipotelefone.getTipo().equals("")) {
            sql += " WHERE tipo=?";
        }

        try {
            openConnection();
            pst = connection.prepareStatement(sql);

            if (tipotelefone.getId() != null && tipotelefone.getTipo().equals("")) {
                pst.setInt(1, tipotelefone.getId());
            } else if (tipotelefone.getId() == null && !tipotelefone.getTipo().equals("")) {
                pst.setString(1, tipotelefone.getTipo());
            }

            ResultSet rs = pst.executeQuery();

            List<EntidadeDominio> tipostelefone = new ArrayList<EntidadeDominio>();
            while (rs.next()) {
                TipoTelefone p = new TipoTelefone();
                p.setId(rs.getInt("id_ttel"));
                p.setTipo(rs.getString("tipotelefone"));

                tipostelefone.add(p);
            }
            return tipostelefone;
        } catch (SQLException e) {
            runRollback(e);
        } finally { // Executa o bloco abaixo independente do que ocorreu
            closeConnection(pst, ctrlTransaction);
        }
        return null;
    }
}
