package lojivro;

import lojivro.aplicacao.Resultado;

public interface Fachada {
    public Resultado salvar(EntidadeDominio entidade);

    public Resultado alterar(EntidadeDominio entidade);

    public Resultado deletar(EntidadeDominio entidade);

    public Resultado consultar(EntidadeDominio entidade);

    public Resultado visuzalizar(EntidadeDominio entidade);
}
