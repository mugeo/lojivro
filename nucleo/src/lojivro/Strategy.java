package lojivro;

public interface Strategy {
    public String processar(EntidadeDominio entidade);
}
