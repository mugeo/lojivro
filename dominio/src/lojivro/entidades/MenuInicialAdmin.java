package lojivro.entidades;

import lojivro.entidades.pessoas.Operador;

public class MenuInicialAdmin extends MenuInicial{
    private Operador operador;

    public Operador getOperador() {
        return operador;
    }

    public void setOperador(Operador operador) {
        this.operador = operador;
    }
}
