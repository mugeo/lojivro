package lojivro.entidades.livro;

import lojivro.EntidadeDominio;

public class Categoria extends EntidadeDominio {
    private String categoria;

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
}
