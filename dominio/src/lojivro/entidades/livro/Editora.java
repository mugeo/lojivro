package lojivro.entidades.livro;

import lojivro.EntidadeDominio;

public class Editora extends EntidadeDominio {
    private String nomeEditora;
    //____________________methods

    public String getNomeEditora() {
        return nomeEditora;
    }

    public void setNomeEditora(String nomeEditora) {
        this.nomeEditora = nomeEditora;
    }


}
