package lojivro.entidades.livro;

import lojivro.EntidadeDominio;

import java.util.List;

public class Livro extends EntidadeDominio {
    private String titulo;
    private String sinopse;
    private List<CategoriaLivro> categorias;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSinopse() {
        return sinopse;
    }

    public void setSinopse(String sinopse) {
        this.sinopse = sinopse;
    }

    public List<CategoriaLivro> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<CategoriaLivro> categorias) {
        this.categorias = categorias;
    }
}