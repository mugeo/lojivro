package lojivro.entidades.livro;

import lojivro.EntidadeDominio;
import lojivro.entidades.pessoas.Pessoa;

import java.util.List;

public class Autor extends Pessoa {
    private String nacionalidade;
    private String descricao;
    private List<Edicao> livros;

    public String getNacionalidade() {
        return nacionalidade;
    }

    public void setNacionalidade(String nacionalidade) {
        this.nacionalidade = nacionalidade;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<Edicao> getLivros() {
        return livros;
    }

    public void setLivros(List<Edicao> livros) {
        this.livros = livros;
    }
}
