package lojivro.entidades.livro;

import lojivro.EntidadeDominio;
import lojivro.entidades.acoes.Ativacao;
import lojivro.entidades.acoes.Desativacao;

import java.util.List;

public class Edicao extends EntidadeDominio {
    private Livro livro;
    private List<Imagem> imagens;
    private String anoLancamento;
    private String edicao;
    private String isbn;
    private int numeroPaginas;
    private String codigoBarras;
    private Double precoCusto;
    private Double precoVenda;
    private boolean status;

    private Autor autor;
    private Editora editora;
    private GrupoPrecificacao grupoPrecificacao;
    private Dimencoes dimencoes;
    private List<Ativacao> ativacoes;
    private List<Desativacao> desativacoes;

    public Livro getLivro() {
        return livro;
    }

    public void setLivro(Livro livro) {
        this.livro = livro;
    }

    public String getAnoLancamento() {
        return anoLancamento;
    }

    public void setAnoLancamento(String anoLancamento) {
        this.anoLancamento = anoLancamento;
    }

    public String getEdicao() {
        return edicao;
    }

    public void setEdicao(String edicao) {
        this.edicao = edicao;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public int getNumeroPaginas() {
        return numeroPaginas;
    }

    public void setNumeroPaginas(int numeroPaginas) {
        this.numeroPaginas = numeroPaginas;
    }

    public String getCodigoBarras() {
        return codigoBarras;
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    public Double getPrecoCusto() {
        return precoCusto;
    }

    public void setPrecoCusto(Double precoCusto) {
        this.precoCusto = precoCusto;
    }

    public Double getPrecoVenda() {
        return precoVenda;
    }

    public void setPrecoVenda(Double precoVenda) {
        this.precoVenda = precoVenda;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Autor getAutor() {
        return autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    public Editora getEditora() {
        return editora;
    }

    public void setEditora(Editora editora) {
        this.editora = editora;
    }

    public GrupoPrecificacao getGrupoPrecificacao() {
        return grupoPrecificacao;
    }

    public void setGrupoPrecificacao(GrupoPrecificacao grupoPrecificacao) {
        this.grupoPrecificacao = grupoPrecificacao;
    }

    public Dimencoes getDimencoes() {
        return dimencoes;
    }

    public void setDimencoes(Dimencoes dimencoes) {
        this.dimencoes = dimencoes;
    }

    public List<Ativacao> getAtivacoes() {
        return ativacoes;
    }

    public void setAtivacoes(List<Ativacao> ativacoes) {
        this.ativacoes = ativacoes;
    }

    public List<Desativacao> getDesativacoes() {
        return desativacoes;
    }

    public void setDesativacoes(List<Desativacao> desativacoes) {
        this.desativacoes = desativacoes;
    }

    public List<Imagem> getImagens() {
        return imagens;
    }

    public void setImagens(List<Imagem> imagens) {
        this.imagens = imagens;
    }
}
