package lojivro.entidades;

import lojivro.entidades.pessoas.Cliente;

public class MenuInicialCliente extends MenuInicial {
    private Cliente cliente;

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
