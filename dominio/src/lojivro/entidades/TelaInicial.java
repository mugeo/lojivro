package lojivro.entidades;

import lojivro.EntidadeDominio;
import lojivro.entidades.livro.Categoria;
import lojivro.entidades.livro.Edicao;
import lojivro.entidades.pessoas.Cliente;

import java.util.List;

public class TelaInicial extends EntidadeDominio {
    private List<Edicao> edicoes;

    public List<Edicao> getEdicoes() {
        return edicoes;
    }

    public void setEdicoes(List<Edicao> edicoes) {
        this.edicoes = edicoes;
    }
}
