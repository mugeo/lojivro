/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lojivro.entidades.acoes;

import java.util.Date;
import lojivro.EntidadeDominio;
import lojivro.entidades.livro.Categoria;

/**
 *
 * @author muril
 */
public class Desativacao extends EntidadeDominio{
    private String motivo;
    private CategoriaDesativacao categoriaDesativacao;
    private Date dtDesativacao;

    /**
     * @return the motivo
     */
    public String getMotivo() {
        return motivo;
    }

    /**
     * @param motivo the motivo to set
     */
    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    /**
     * @return the dtDesativacao
     */
    public Date getDtDesativacao() {
        return dtDesativacao;
    }

    /**
     * @param dtDesativacao the dtDesativacao to set
     */
    public void setDtDesativacao(Date dtDesativacao) {
        this.dtDesativacao = dtDesativacao;
    }
}
