/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lojivro.entidades.acoes;

import java.util.Date;
import lojivro.EntidadeDominio;

/**
 *
 * @author muril
 */
public class Ativacao extends EntidadeDominio{
    private String motivo;
    private CategoriaAtivacao categoriaAtivacao;
    private Date dtAtivacao;

    /**
     * @return the motivo
     */
    public String getMotivo() {
        return motivo;
    }

    /**
     * @param motivo the motivo to set
     */
    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    /**
     * @return the dtAtivacao
     */
    public Date getDtAtivacao() {
        return dtAtivacao;
    }

    /**
     * @param dtAtivacao the dtAtivacao to set
     */
    public void setDtAtivacao(Date dtAtivacao) {
        this.dtAtivacao = dtAtivacao;
    }
}
