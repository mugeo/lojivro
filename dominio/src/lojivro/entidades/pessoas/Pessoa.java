package lojivro.entidades.pessoas;
import java.util.Date;
import lojivro.EntidadeDominio;
public class Pessoa extends EntidadeDominio{
    private String nome;
    private Date dataNasc;

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the dataNasc
     */
    public Date getDataNasc() {
        return dataNasc;
    }

    /**
     * @param dataNasc the dataNasc to set
     */
    public void setDataNasc(Date dataNasc) {
        this.dataNasc = dataNasc;
    }
    
}
