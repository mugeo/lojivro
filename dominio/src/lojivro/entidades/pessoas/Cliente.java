/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lojivro.entidades.pessoas;

import java.util.List;
import lojivro.entidades.acoes.Ativacao;
import lojivro.entidades.acoes.Desativacao;
import lojivro.entidades.infopessoas.cartoes.CartaoCredito;
import lojivro.entidades.infopessoas.contato.Telefone;
import lojivro.entidades.infopessoas.endereco.Endereco;

/**
 *
 * @author muril
 */
public class Cliente extends Pessoa{
    private String cpf;
    private String genero;
    private Usuario usuario;
    private List<Telefone> telefones;
    private List<Endereco> enderecos;
    private List<CartaoCredito> cartoes;
    private List<Ativacao> ativacoes;
    private List<Desativacao> desativacoes;

    /**
     * @return the cpf
     */
    public String getCpf() {
        return cpf;
    }

    /**
     * @param cpf the cpf to set
     */
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    /**
     * @return the genero
     */
    public String getGenero() {
        return genero;
    }

    /**
     * @param genero the genero to set
     */
    public void setGenero(String genero) {
        this.genero = genero;
    }

    /**
     * @return the usuario
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the telefones
     */
    public List<Telefone> getTelefones() {
        return telefones;
    }

    /**
     * @param telefones the telefones to set
     */
    public void setTelefones(List<Telefone> telefones) {
        this.telefones = telefones;
    }

    /**
     * @return the enderecos
     */
    public List<Endereco> getEnderecos() {
        return enderecos;
    }

    /**
     * @param enderecos the enderecos to set
     */
    public void setEnderecos(List<Endereco> enderecos) {
        this.enderecos = enderecos;
    }

    /**
     * @return the cartoes
     */
    public List<CartaoCredito> getCartoes() {
        return cartoes;
    }

    /**
     * @param cartoes the cartoes to set
     */
    public void setCartoes(List<CartaoCredito> cartoes) {
        this.cartoes = cartoes;
    }

    /**
     * @return the ativacoes
     */
    public List<Ativacao> getAtivacoes() {
        return ativacoes;
    }

    /**
     * @param ativacoes the ativacoes to set
     */
    public void setAtivacoes(List<Ativacao> ativacoes) {
        this.ativacoes = ativacoes;
    }

    /**
     * @return the desativacoes
     */
    public List<Desativacao> getDesativacoes() {
        return desativacoes;
    }

    /**
     * @param desativacoes the desativacoes to set
     */
    public void setDesativacoes(List<Desativacao> desativacoes) {
        this.desativacoes = desativacoes;
    }
    
    
}
