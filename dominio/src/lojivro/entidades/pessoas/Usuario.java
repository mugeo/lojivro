/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lojivro.entidades.pessoas;

import java.util.Date;
import lojivro.EntidadeDominio;

/**
 *
 * @author muril
 */
public class Usuario extends EntidadeDominio{

    private String email;
    private String senha;
    private String reSenha;
    private Date dataCadastro;
    private boolean status;
    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the senha
     */
    public String getSenha() {
        return senha;
    }

    /**
     * @param senha the senha to set
     */
    public void setSenha(String senha) {
        this.senha = senha;
    }

    /**
     * @return the reSenha
     */
    public String getReSenha() {
        return reSenha;
    }

    /**
     * @param reSenha the reSenha to set
     */
    public void setReSenha(String reSenha) {
        this.reSenha = reSenha;
    }

    /**
     * @return the dataCadastro
     */
    public Date getDataCadastro() {
        return dataCadastro;
    }

    /**
     * @param dataCadastro the dataCadastro to set
     */
    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
