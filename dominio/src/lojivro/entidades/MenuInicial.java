package lojivro.entidades;

import lojivro.EntidadeDominio;
import lojivro.entidades.livro.CategoriaLivro;

import java.util.List;

public class MenuInicial extends EntidadeDominio {
    private List<CategoriaLivro> categorias;

    public List<CategoriaLivro> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<CategoriaLivro> categorias) {
        this.categorias = categorias;
    }
}
