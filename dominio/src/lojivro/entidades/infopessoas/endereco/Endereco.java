/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lojivro.entidades.infopessoas.endereco;

import lojivro.EntidadeDominio;

/**
 *
 * @author muril
 */
public class Endereco extends EntidadeDominio{
    private TipoResidencia tipoResidencia;
    private String numero;
    private Localizacao localizacao;
    private String observacoes;

    /**
     * @return the tipoResidencia
     */
    public TipoResidencia getTipoResidencia() {
        return tipoResidencia;
    }

    /**
     * @param tipoResidencia the tipoResidencia to set
     */
    public void setTipoResidencia(TipoResidencia tipoResidencia) {
        this.tipoResidencia = tipoResidencia;
    }

    /**
     * @return the numero
     */
    public String getNumero() {
        return numero;
    }

    /**
     * @param numero the numero to set
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * @return the localizacao
     */
    public Localizacao getLocalizacao() {
        return localizacao;
    }

    /**
     * @param localizacao the localizacao to set
     */
    public void setLocalizacao(Localizacao localizacao) {
        this.localizacao = localizacao;
    }

    /**
     * @return the observacoes
     */
    public String getObservacoes() {
        return observacoes;
    }

    /**
     * @param observacoes the observacoes to set
     */
    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }
}
