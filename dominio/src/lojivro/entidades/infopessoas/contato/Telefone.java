package lojivro.entidades.infopessoas.contato;

import lojivro.EntidadeDominio;

public class Telefone extends EntidadeDominio{
    private TipoTelefone tipoTelefone;
    private int ddd;
    private String telefone;

    /**
     * @return the tipoTelefone
     */
    public TipoTelefone getTipoTelefone() {
        return tipoTelefone;
    }

    /**
     * @param tipoTelefone the tipoTelefone to set
     */
    public void setTipoTelefone(TipoTelefone tipoTelefone) {
        this.tipoTelefone = tipoTelefone;
    }

    /**
     * @return the ddd
     */
    public int getDdd() {
        return ddd;
    }

    /**
     * @param ddd the ddd to set
     */
    public void setDdd(int ddd) {
        this.ddd = ddd;
    }

    /**
     * @return the telefone
     */
    public String getTelefone() {
        return telefone;
    }

    /**
     * @param telefone the telefone to set
     */
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
