/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lojivro.entidades.infopessoas.cartoes;

import lojivro.EntidadeDominio;

/**
 *
 * @author muril
 */
public class Bandeira extends EntidadeDominio{
    private String bandeira;

    /**
     * @return the bandeira
     */
    public String getBandeira() {
        return bandeira;
    }

    /**
     * @param bandeira the bandeira to set
     */
    public void setBandeira(String bandeira) {
        this.bandeira = bandeira;
    }
    
    
}
