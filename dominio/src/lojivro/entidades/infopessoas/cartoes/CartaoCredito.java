/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lojivro.entidades.infopessoas.cartoes;

import lojivro.EntidadeDominio;

/**
 *
 * @author muril
 */
public class CartaoCredito extends EntidadeDominio{
    private Bandeira bandeira;
    private String numero;
    private int mes;
    private int ano;
    private String nomeImpresso;
    private String codSeguranca;

    /**
     * @return the bandeira
     */
    public Bandeira getBandeira() {
        return bandeira;
    }

    /**
     * @param bandeira the bandeira to set
     */
    public void setBandeira(Bandeira bandeira) {
        this.bandeira = bandeira;
    }

    /**
     * @return the numero
     */
    public String getNumero() {
        return numero;
    }

    /**
     * @param numero the numero to set
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * @return the mes
     */
    public int getMes() {
        return mes;
    }

    /**
     * @param mes the mes to set
     */
    public void setMes(int mes) {
        this.mes = mes;
    }

    /**
     * @return the ano
     */
    public int getAno() {
        return ano;
    }

    /**
     * @param ano the ano to set
     */
    public void setAno(int ano) {
        this.ano = ano;
    }

    /**
     * @return the nomeImpresso
     */
    public String getNomeImpresso() {
        return nomeImpresso;
    }

    /**
     * @param nomeImpresso the nomeImpresso to set
     */
    public void setNomeImpresso(String nomeImpresso) {
        this.nomeImpresso = nomeImpresso;
    }

    /**
     * @return the codSeguranca
     */
    public String getCodSeguranca() {
        return codSeguranca;
    }

    /**
     * @param codSeguranca the codSeguranca to set
     */
    public void setCodSeguranca(String codSeguranca) {
        this.codSeguranca = codSeguranca;
    }
    
    
}
