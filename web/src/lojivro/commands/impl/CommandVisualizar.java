package lojivro.commands.impl;

import lojivro.EntidadeDominio;
import lojivro.aplicacao.Resultado;

public class CommandVisualizar extends CommandAbs {
    @Override
    public Resultado executar(EntidadeDominio entidade) {
        return fachada.visuzalizar(entidade);
    }
}
