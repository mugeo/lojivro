package lojivro.commands;

import lojivro.EntidadeDominio;
import lojivro.aplicacao.Resultado;

public interface Command {
    public Resultado executar(EntidadeDominio entidade);
}
