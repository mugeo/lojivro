package lojivro.viewhelper.impl;

import lojivro.EntidadeDominio;
import lojivro.aplicacao.Resultado;
import lojivro.impl.dao.DaoLivro;
import lojivro.entidades.livro.*;
import lojivro.viewhelper.ViewHelper;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class LivroViewHelper implements ViewHelper {

    public EntidadeDominio getEntidade(HttpServletRequest request) {
        // TODO Auto-generated method stub
        String operacao = request.getParameter("operacao");

        Livro livro = null; // instancia livro
        if (!operacao.equals("VISUALIZAR")) {
            livro = new Livro();
            String id = request.getParameter("idLivro");
            String titulo = request.getParameter("titulo");

            if (id != null && !id.isEmpty()) {
                livro.setId(Integer.parseInt(id));
            }
            if (titulo != null && !titulo.isEmpty()) {
                livro.setTitulo(titulo);
            }
        } else {
            HttpSession session = request.getSession();
            @SuppressWarnings("unchecked")
            List<Livro> livros = (List<Livro>) session.getAttribute("livros");
            String txtId = request.getParameter("idLivro");
            int id = 0;

            if (txtId != null && !txtId.trim().equals("")) {
                id = Integer.parseInt(txtId);
            }

            if (livros != null) {
                for (Livro l : livros) {
                    if (l.getId() == id) {
                        livro = l;
                    }
                }
            } else {
                livro = new Livro();
            }
        }
        return livro;
    }

    public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        RequestDispatcher d = null;

        HttpSession session = request.getSession();
        String operacao = request.getParameter("operacao");

        if (resultado.getMsg() == null && operacao.equals("SALVAR")) {
            resultado.setMsg("Livro cadastrado com sucesso!");

            DaoLivro daoLivro = new DaoLivro(); //nao pode chamar um DAO da parte web
            List<EntidadeDominio> livros = new ArrayList<EntidadeDominio>(); // criando uma lista de livro
            //livros = daoLivro.consultar(null);

            request.getSession().setAttribute("livros", livros);
            d = request.getRequestDispatcher("../Livro/list.jsp");
        }

        if (resultado.getMsg() == null && operacao.equals("ALTERAR")) {
            resultado.setMsg("Livro alterado com sucesso!");

            DaoLivro daoLivro = new DaoLivro();
            List<EntidadeDominio> livros = new ArrayList<EntidadeDominio>(); // criando uma lista de livro
            //livros = daoLivro.consultar(null);

            request.getSession().setAttribute("livros", livros);
            d = request.getRequestDispatcher("../Livro/list.jsp");
        }
        if (resultado.getMsg() == null && operacao.equals("INATIVAR")) {
            resultado.setMsg("Livro inativado com sucesso!");

            DaoLivro daoLivro = new DaoLivro();
            List<EntidadeDominio> livros = new ArrayList<EntidadeDominio>(); // criando uma lista de livro
            //livros = daoLivro.consultar(null);

            request.getSession().setAttribute("livros", livros);
            d = request.getRequestDispatcher("../Livro/list.jsp");
        }

        if (operacao.equals("GETALLBOOKS")) {

            DaoLivro daoLivro = new DaoLivro();
            List<EntidadeDominio> livros = new ArrayList<EntidadeDominio>(); // criando uma lista de livro
            //livros = daoLivro.consultar(null);

            request.getSession().setAttribute("livros", livros);
            d = request.getRequestDispatcher("../Livro/list.jsp");

        }

        if (operacao.equals("NEWBOOK")) {

            loadingForm(request);

            d = request.getRequestDispatcher("insert.jsp");

        }

        if (resultado.getMsg() == null && operacao.equals("VISUALIZAR")) {
            loadingForm(request);
            request.setAttribute("livro", resultado.getEntidades().get(0));
            d = request.getRequestDispatcher("insert.jsp");
        }

        if (resultado.getMsg() == null && operacao.equals("EXCLUIR")) {

            request.getSession().setAttribute("livros", null);
            d = request.getRequestDispatcher("list.jsp");
        }

        // if (resultado.getMsg() != null) {
        // if (operacao.equals("SALVAR") || operacao.equals("ALTERAR")) {
        // request.getSession().setAttribute("livros", resultado);
        // d = request.getRequestDispatcher("list.jsp");
        // }
        // }

        d.forward(request, response);

    }

    public void loadingForm(HttpServletRequest request) {
        DaoLivro livrodao = new DaoLivro();
        List<EntidadeDominio> autores = new ArrayList<EntidadeDominio>();
        List<EntidadeDominio> editoras = new ArrayList<EntidadeDominio>();
        List<EntidadeDominio> categorias = new ArrayList<EntidadeDominio>();
        List<EntidadeDominio> grupos = new ArrayList<EntidadeDominio>();

        //autores = livrodao.getAutor(null);
        //editoras = livrodao.getEditora(null);
        //categorias = livrodao.getCategoria(null);
        //grupos = livrodao.getGrupos(null);

        request.getSession().setAttribute("autores", autores);
        request.getSession().setAttribute("editoras", editoras);
        request.getSession().setAttribute("categorias", categorias);
        request.getSession().setAttribute("grupos", grupos);
    }

}