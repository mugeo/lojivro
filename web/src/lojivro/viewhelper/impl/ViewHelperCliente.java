
package lojivro.viewhelper.impl;

import lojivro.EntidadeDominio;
import lojivro.aplicacao.Resultado;
import lojivro.entidades.pessoas.Cliente;
import lojivro.viewhelper.ViewHelper;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class ViewHelperCliente implements ViewHelper {


    public EntidadeDominio getEntidade(HttpServletRequest request, HttpServletResponse response) {
        String nome = request.getParameter("txtNome");
        String cpf = request.getParameter("txtCpf");
        String id = request.getParameter("txtId");


        Cliente c = new Cliente();
        c.setNome(nome);

        if (id != null && !id.trim().equals("")) {
            c.setId(Integer.parseInt(id));
        }

        c.setCpf(cpf);
        return c;
    }


    public void setView(Resultado resultado, HttpServletRequest request,
                        HttpServletResponse response) {
        // TODO Auto-generated method stub
        RequestDispatcher rd;
        request.getSession().setAttribute("resultado", resultado);
        rd = request.getRequestDispatcher("visualizarCliente.jsp");
        try {
            rd.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
