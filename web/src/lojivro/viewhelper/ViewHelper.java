package lojivro.viewhelper;

import lojivro.EntidadeDominio;
import lojivro.aplicacao.Resultado;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface ViewHelper {
    public EntidadeDominio getEntidade(HttpServletRequest request, HttpServletResponse response);

    public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException;
}
