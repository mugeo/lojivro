package lojivro;

import lojivro.aplicacao.Resultado;
import lojivro.commands.Command;
import lojivro.commands.impl.*;
import lojivro.viewhelper.ViewHelper;
import lojivro.viewhelper.impl.ViewHelperCliente;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "Servlet")
public class Servlet extends HttpServlet {
    //Criando Mapa de Commands, classes que chamam a fachada.
    private Map<String, Command> commands;
    //Criando Mapa de View Helpers, classes que criam objetos e modificam a view
    private Map<String, ViewHelper> viewHelpers;


    //Criando construtor da Classe
    //Sempre que a servlet for chamada, uma intancia da classe será criada
    //Para criação da instacia este construtor será chamado e as ações dentro dela executadas.
    public Servlet() {
        //Instanciando objeto de Hashmao no mapa de commands criado
        commands = new HashMap<String, Command>();
        //Instanciando objeto de Hashmao no mapa de viewhelpers
        viewHelpers = new HashMap<String, ViewHelper>();

        //inserindo no mapa de commands as chamadas
        //Tudo que esta no mapa é referenciado por uma String
        //Para cada String é necessario uma chamada de instancia

        //Caso seja chamado o mapa de command com uma referencia de String "Salvar"
        //Uma instacia da classe CommandSalvar será retornada
        commands.put("Salvar", new CommandSalvar());
        //O mesmo vale para Alterar
        commands.put("Alterar", new CommandAlterar());
        //Deletar
        commands.put("Deletar", new CommandDeletar());
        //Consultar
        commands.put("Consultar", new CommandConsultar());
        //Visualizar
        commands.put("Visuzalizar", new CommandVisualizar());

        //inserindo no mapa de viewHelpers as chamadas
        //Tudo que esta no mapa é referenciado por uma String
        //Para cada String é necessario uma chamada de um instancia

        //Caso seja chamado o mapa de command com uma referencia de String "Livro"
        //Uma instacia da classe ViewHelperLivro será retornada
        viewHelpers.put("/Cliente", new ViewHelperCliente());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doProcessRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doProcessRequest(request, response);
    }

    private void doProcessRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        //Adiquirindo o parametro de ação do request, este vamos usar para chamar a Command correta
        //Está operação é declarada no botão da pagina
        String operacao = request.getParameter("operacao");
        //Adiquirindo o parametro de URI do request, este vamos usar para chamar a ViewHelper correta
        //Está URI é o endereço após o nome do servidor, é definida no action do form
        String uri = request.getRequestURI();
        //Primeiro vamos instaciar uma variavel do tipo interface ViewHelper
        //     e utilizar nosso Mapa de ViewHelpers para instaciar um objeto nesta variavel
        ViewHelper viewHelper = viewHelpers.get(uri);
        //Em seguida iremos criar um objeto do tipo EntidadeDominio
        //     a classe que é extendida por todas as outras classes de dominio
        //     esta nossa instancia vai receber um objeto criado detro das viewhelpers pelo metodo getEntidade
        EntidadeDominio entidade = viewHelper.getEntidade(request, response);
        //Adiquirida nossa entidade vamos criar uma variavel do tipo Interface command
        //E vamos usar o nosso mapa de Commands indexado pela nossa operação
        //     para instaciar um objeto de alguma classe que implemente a interface Command.
        Command command = commands.get(operacao);
        //Agora vamos chamar o metodo command usando nossa entidade como parametro.
        //Este método vai retornar um resultado, então vamos criar uma instacia de Resultado para obte-lo
        Resultado resultado = command.executar(entidade);
        //Por fim vamos chamar o setview também da viewHelper para modificar nossa visualização.
        //Usando como paramentro nosso request, response o objeto de resultado que obtivemos.
        viewHelper.setView(resultado, request, response);
    }
}
