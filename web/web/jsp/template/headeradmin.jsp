<%@ page import="lojivro.entidades.pessoas.Cliente" %>
<%@ page import="lojivro.aplicacao.Resultado" %>
<%@ page import="lojivro.commands.impl.CommandConsultar" %>
<%@ page import="lojivro.entidades.MenuInicialAdmin" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
	MenuInicialAdmin menuInicialAdmin = new MenuInicialAdmin();
	Resultado resultado = null;
	if (request.getSession().getAttribute("menuinicialadmin") != null) {
		resultado = (Resultado) request.getSession().getAttribute("menuinicialadmin");
		menuInicialAdmin = (MenuInicialAdmin) resultado.getEntidades().get(0);
	} else {
		CommandConsultar commandConsultar = new CommandConsultar();
		resultado = commandConsultar.executar(menuInicialAdmin);
		menuInicialAdmin = (MenuInicialAdmin) resultado.getEntidades().get(0);
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Lojivro</title>
	<link rel="stylesheet" href="https://bootswatch.com/3/sandstone/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
<jsp:useBean id="categorial" class="lojivro.entidades.livro.CategoriaLivro"/>
<jsp:useBean id="menuInicialAdmin" class="lojivro.entidades.MenuInicialAdmin"/>
<jsp:useBean id="cliente" class="lojivro.entidades.pessoas.Cliente"/>
<header class="main-header">
	Lojivro
</header>
<nav class="navbar navbar-nav">
	<div class="container">
		<div class="navbar-header">
			<a href="index.jsp" class="navbar-brand"><b>Lojivro</b></a>
			<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#MyNavbar">
				<i class="fa fa-bars"></i>
			</button>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse"
			 id="MyNavbar">
			<ul class="nav navbar-nav">
				<li><a href="index.jsp">Home</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Categorias
						<span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<c:forEach var="categorial" items="${menuInicialAdmin.categorias}">
							<li>
								<a href="./Edicao?operacao=Consultar&Categoria=${categorial.categoria}">${categorial.categoria}</a>
							</li>
						</c:forEach>
					</ul>
				</li>
			</ul>
			<!-- search form (Optional)
            <form action="./Edicao" method="post"
                  class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input type="text" id="navbar-search-input" name="nome_pet"
                           class="form-control" placeholder="Pesquisar...">
                    <button type="submit" name="operacao" id="operacao"
                            value="CONSULTAR" class="btn btn-default">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </form>-->
			<!-- /.search form -->
			<ul class="nav navbar-nav navbar-right">
				<%
					if (request.getSession().getAttribute("cliente") != null) {
						cliente = (Cliente) request.getSession().getAttribute("cliente");
				%>
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Bem
					vindo, ${cliente.nome}<span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="meuperfil.jsp">MeuPerfil</a></li>
					<li class="divider"></li>
					<li><a href="logout.jsp">Logout</a></li>
				</ul>
				<%
				} else {
					    response.sendRedirect("login.jsp");
					}
				%>
			</ul>
		</div>
	</div>
</nav>