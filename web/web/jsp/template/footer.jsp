
<footer class="main-footer">
	<div class="container">
		<div class="pull-right hidden-xs">
			<b>Versão</b> 1.0.0
		</div>
		<strong>Copyright &copy; 2018 <a
			href="https://mugeo.io">Mugeo Studio</a>.
		</strong> Todos os direitos Reservados
	</div>
	<!-- /.container -->
</footer>
</body>
</html>