<%@ page import="lojivro.EntidadeDominio" %>
<%@ page import="lojivro.commands.impl.CommandConsultar" %>
<%@ page import="lojivro.entidades.livro.CategoriaLivro" %>
<%@ page import="lojivro.entidades.livro.Livro" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<head>
    <title>Lojivro - Cadastro de Livro</title>
</head>
<!-- Header import -->
<c:import url="../../template/headeradmin.jsp"/>
<jsp:useBean id="livro" class="lojivro.entidades.livro.Livro"/>
<jsp:useBean id="categorial" class="lojivro.entidades.livro.CategoriaLivro"/>
<jsp:useBean id="resultado" class="lojivro.aplicacao.Resultado"/>
<jsp:useBean id="command" class="lojivro.commands.impl.CommandConsultar"/>
<c:if test="${request.getSession().getAttribute('livro') != null}">
    <c:set var="livro" value="${request.getSession().getAttribute('livro')}"/>
</c:if>
<c:set var="command" value="${CommandConsultar()}"/>
<c:set var="resultado" value="${command.executar(categorial)}"/>
<section>
    <article>
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-defaul">
                    <!-- form start -->
                    <form role="form" action="./Livro" method="post"
                          class="form-horizontal">
                        <fieldset>
                            <legend>
                                <c:if test="${livro != null }">
                                    <h1>Atualizar Livro</h1>
                                </c:if>
                                <c:if test="${livro == null }">
                                    <h1>Cadastro de Livro</h1>
                                </c:if>
                            </legend>
                            <div class="box-body">
                                <div class="form-group">
                                    <c:if test="${livro != null }">
                                        <label for="id" class="control-label col-md-2">Id:</label>
                                    </c:if>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="id"
                                               name="id" value="${livro.id}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">T�tulo do Livro</label>
                                    <div class="col-md-10">
                                        <input required type="text" class="form-control" id="titulo"
                                               name="titulo" placeholder="Entre com nome do livro"
                                               value="${livro.titulo}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Sinopse</label>
                                    <div class="col-md-10">
                                        <input required type="text" class="form-control" id="sinopse"
                                               name="sinopse" placeholder="Entre com a sinopse livro"
                                               value="${livro.sinopse}">
                                    </div>
                                </div>

                                <c:if test="${livro != null }">
                                    <div class="form-group">
                                        <label for="dtcadastro" class="col-md-2 control-label">Data Cadastro</label>
                                        <div class="col-md-10">
                                            <input required type="datetime-local" class="form-control" id="dtcadastro"
                                                   name="dtcadastro"
                                                   value="${livro.dtCadastro}">
                                        </div>
                                    </div>
                                </c:if>
                                <div class="form-group">
                                    <label for="categoria" class="col-sm-2 control-label">Categoria</label>
                                    <div class="col-sm-4">
                                        <select required class="form-control" id="categoria"
                                                name="categoria" multiple>
                                            <%
                                                for (EntidadeDominio ed : resultado.getEntidades()) {
                                                    categorial = (CategoriaLivro) ed;
                                            %>
                                            <option value="${categorial.id}"
                                                    <c:forEach items="${livro.categorias}" var="categoria">
                                                        <c:if test="${categoria.id == categorial.id}">
                                                            <%=" selected"%>
                                                        </c:if>
                                                    </c:forEach>
                                            >${categorial.categoria}</option>
                                            <%
                                                }
                                            %>
                                        </select>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <c:if test="${livro != null}">
                                        <button type='submit' class='btn btn-alert' id='operacao'
                                                name='operacao' value='Alterar'>ALTERAR
                                        </button>

                                    </c:if>
                                    <c:if test="${livro == null}">
                                        <button type='submit' class='btn btn-default' id='operacao'
                                                name='operacao' value='Salvar'>SALVAR
                                        </button>
                                    </c:if>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </article>
</section>
<!-- Footer import -->
<c:import url="../../template/footeradmin.jsp"></c:import>