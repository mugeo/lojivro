<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: geovane.junior
  Date: 13/05/2018
  Time: 09:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Lojivro</title>
    <link rel="stylesheet" href="https://bootswatch.com/3/sandstone/bootstrap.min.css"/>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<%--<c:import url="../../template/headeradmin.jsp"/>--%>
<section>
    <article>
        <div class="row"></div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <form action="./Autor" method="post">
                    <fieldset>
                        <legend>Inserir novo Autor</legend>
                        <div class="form-group row">
                            <label for="nome" class="control-label col-md-12">Nome</label>
                            <div class="col-md-12">
                                <input type="text" id="nome" name="nome" class="control-group"
                                       placeholder="Nome do Autor...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="data" class="control-label col-md-12">Data de Nascimento</label>
                            <div class="col-md-12">
                                <input type="date" id="data" name="data" class="control-group">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nacionalidade" class="control-label col-md-12">Nascionalidade</label>
                            <div class="col-md-12">
                                <select id="nacionalidade" name="nacionalidade" class="control-group">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="descricao" class="control-label col-md-12">Descrição</label>
                            <div class="col-md-12">
                                <textarea id="descricao" name="descricao" class="control-group"
                                          placeholder="Descrição sobre o autor"></textarea>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary" value="Salvar" name="operacao"
                                id="operacao_salvar">Salvar
                        </button>
                    </fieldset>
                </form>
            </div>
            <div class="col-md-3"></div>
        </div>
    </article>
</section>
<%--<c:import url="../../template/footeradmin.jsp"/>--%>
<script id="tmplNacionalidades" type="text/x-jquery-tmpl">
    <option value="$(gentilico)"> $(gentilico) </option>

</script>
<script>
    $(document).ready(function () {
        $(function () {
            $.getJSON("../../resources/nacionalidades.json", function (nacionalidade) {
                $('#tmplNacionalidades').template('meuTemplate');
                $.tmpl('meuTemplate', nacionalidade.nacionalidades).append('#nacionalidade');
            });
        });
    });
</script>
</body>
</html>
