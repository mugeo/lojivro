<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<head>
<title>Lojivro - Cadastro de Livro</title>
</head>
<!--  Header import -->
<c:import url="../../template/headeradmin.jsp"></c:import>
<jsp:useBean id="livro" class="lojivro.entidades.livro.Livro" scope="session"/>

<section>
	<article>
		<div class="row">
			<!-- left column -->
			<div class="col-md-12">
				<!-- general form elements -->
				<div class="box box-defaul">
					<!-- form start -->
					<form role="form" action="./Livro" method="post"
						class="form-horizontal">
						<fieldset>
							<legend>
								<c:if test="${livro != null }">
									<h1>Atualizar Livro</h1>
								</c:if>
								<c:if test="${livro == null }">
									<h1>Cadastro de Livro</h1>
								</c:if>
							</legend>
						<div class="box-body">
							<div class="form-group row">
                                <c:if test="${livro != null }">
                                    <label for="id" class="control-label col-md-2">Id:</label>
                                </c:if>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="id"
                                           name="id" value="${livro.id}">
                                </div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">T�tulo do Livro</label>
								<div class="col-sm-10">
									<input required type="text" class="form-control" id="titulo"
										name="titulo" placeholder="Entre com nome do livro"
										value="${livro.titulo}">
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">Sinopse</label>
								<div class="col-sm-10">
									<input required type="text" class="form-control" id="sinopse"
										name="sinopse" placeholder="Entre com a sinopse livro"
										value="${livro.sinopse}">
								</div>
							</div>

                            <c:if test="${livro != null }">
                                <div class="form-group">
                                    <label for="dtcadastro" class="col-md-2 control-label">Data Cadastro</label>
                                    <div class="col-md-10">
                                        <input required type="datetime-local" class="form-control" id="dtcadastro"
                                               name="dtcadastro"
                                               value="${livro.dtCadastro}">
                                    </div>
                                </div>
                            </c:if>


							<div class="form-group">
								<label class="col-sm-2 control-label">Editora</label>
								<div class="col-sm-4">
									<select required class="form-control select2" id="editora"
										name="editora">
										<c:forEach items="${editoras}" var="e">
											<option value="${e.getId()}"
												${livro.getEditora().getId() == e.getId() ? 'selected' : ''}>${e.getNomeEditora()}</option>
										</c:forEach>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">Edi��o</label>
								<div class="col-sm-10">
									<input required type="number" class="form-control" id="edicao"
										name="edicao" placeholder="Entre com a edi��o livro"
										value="${livro.getEdicao()}">
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">Ano</label>
								<div class="col-sm-10">
									<input required type="number" class="form-control" id="ano"
										name="ano" placeholder="Entre com o ano do livro"
										value="${livro.getAno()}">
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">ISBN</label>
								<div class="col-sm-10">
									<input required type="text" class="form-control" id="isbn"
										name="isbn" placeholder="Entre com a ISBN do livro"
										value="${livro.getIsbn()}">
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">C�digo de barras</label>
								<div class="col-sm-10">
									<input required type="text" class="form-control"
										id="codigoBarras" name="codigoBarras"
										placeholder="Entre com o c�digo de barras do livro"
										value="${livro.getCodigoBarras()}">
								</div>
							</div>

							

							<div class="form-group">
								<label class="col-sm-2 control-label">Status</label>
								<div class="col-sm-10">
									<div class="radio">
										<label><input
											${livro.isStatus() == true ? 'checked' : '' }
											type="radio" name="status" value="true">Ativo</label>
									</div>
									<div class="radio">
										<label><input
											${livro.isStatus() == false ? 'checked' : '' }
											type="radio" name="status" value="false">Inativo</label>
									</div>
								</div>
							</div>



							<div class="form-group">
								<label class="col-sm-2 control-label">Altura</label>
								<div class="col-sm-10">
									<input required type="number" step=".01" class="form-control"
										id="altura" name="altura"
										placeholder="Entre com a altura livro"
										value="${livro.getAltura()}">
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">Largura</label>
								<div class="col-sm-10">
									<input required type="number" step=".01" class="form-control"
										id="largura" name="largura"
										placeholder="Entre com a largura do livro"
										value="${livro.getLargura()}">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-2 control-label">Peso</label>
								<div class="col-sm-10">
									<input required type="number" step=".01" class="form-control"
										id="peso" name="peso"
										placeholder="Entre com a peso do livro"
										value="${livro.getPeso()}">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-2 control-label">Profundidade</label>
								<div class="col-sm-10">
									<input required type="number" step=".01" class="form-control"
										id="profundidade" name="profundidade"
										placeholder="Entre com a profundidade do livro"
										value="${livro.getProfundidade()}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Grupo de
									Precifica��o</label>
								<div class="col-sm-4">
									<select required class="form-control select2"
										id="grupoPrecificacao" name="grupoPrecificacao">
										<c:forEach items="${grupos}" var="g">								
											<option value="${g.getId()}" ${g.getId() == livro.getGrupoPrecificacao().getId() ? 'selected' : ''}>${g.getNomeGrupo()} - ${g.getPorcentagemLucro()}</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Categoria</label>
								<div class="col-sm-4">
									<select required class="form-control select2" id="categoria"
										name="categoria">
										<c:forEach items="${categorias}" var="c">
											<option value="${c.getId()}"
												${livro.getCategoriaLivro().getId() == c.getId() ? 'selected' : ''}>${c.getNomeCategoria()}</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="box-footer">
								<c:if test="${livro != null}">
									<button type='submit' class='btn btn-default' id='operacao'
										name='operacao' value='ALTERAR'>ALTERAR</button>

								</c:if>
								<c:if test="${livro == null}">
									<button type='submit' class='btn btn-default' id='operacao'
										name='operacao' value='SALVAR'>SALVAR</button>
								</c:if>
							</div>




						</div>
						</fieldset>
					</form>
				</div>
				<!-- /.box -->
			</div>
			<!--/.col (right) -->
		</div>
		<!-- /.row -->
	</article>
		</section>
<!--  Footer import -->
<c:import url="../../template/footeradmin.jsp"></c:import>