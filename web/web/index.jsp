<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="lojivro.aplicacao.Resultado" %>
<%@ page import="lojivro.entidades.TelaInicial" %>
<%@ page import="lojivro.commands.impl.CommandConsultar" %>
<%--
  Created by IntelliJ IDEA.
  User: geovane.junior
  Date: 21/04/2018
  Time: 23:58
  To change this template use File | Settings | File Templates.
--%>
<c:import url="./template/header.jsp"></c:import>
<%
    TelaInicial telaInicial = new TelaInicial();
    Resultado resultado = null;
    if (request.getSession().getAttribute("telainicial") != null){
        resultado = (Resultado) request.getSession().getAttribute("telainicial");
        telaInicial = (TelaInicial) resultado.getEntidades().get(0);
    }else{
        CommandConsultar commandConsultar = new CommandConsultar();
        resultado = commandConsultar.executar(telaInicial);
        telaInicial = (TelaInicial) resultado.getEntidades().get(0);
    }
%>
<jsp:useBean id="telaInicial" class="lojivro.entidades.TelaInicial"/>
<jsp:useBean id="categorial" class="lojivro.entidades.livro.CategoriaLivro"/>
<section>
    <article>
        <div class="row">
            <c:forEach var="edicao" items="${telaInicial.edicoes}">
                <div class="col-md-3">
                    <picture>
                        <img src="./img/livros/${edicao.imagens.get(0)}" alt="${edicao.livro.titulo}">
                    </picture>
                    <h3>${edicao.livro.titulo}</h3>
                    <h4>${edicao.precoVenda}</h4>
                    <a href="./Carrinho?${edicao.id}" class="btn btn-success">Adicionar ao Carrinho</a>
                    <h6>
                        <c:forEach var="categorial" items="${edicao.categorias}">
                            <%=categorial.getCategoria()%>
                            <%=" / "%>
                        </c:forEach>
                    </h6>
                </div>
            </c:forEach>
        </div>
    </article>
</section>
<c:import url="./template/footer.jsp"></c:import>